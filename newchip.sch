EESchema Schematic File Version 2  date Tue 20 Aug 2013 04:01:19 PM MDT
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MCP3004
LIBS:MCP3208
LIBS:Carlolib-dev
LIBS:o_device
LIBS:MCP3002
LIBS:newsensor-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "20 aug 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7000 3300 7000 3400
Wire Wire Line
	7000 3400 6900 3400
Wire Wire Line
	6900 3600 7250 3600
Wire Wire Line
	7250 3600 7250 3500
Wire Wire Line
	7250 3500 7450 3500
Wire Wire Line
	5400 3500 5350 3500
Wire Wire Line
	5350 3500 5350 3200
Wire Wire Line
	5350 3200 5300 3200
Connection ~ 5000 2900
Wire Wire Line
	4650 3600 5000 3600
Wire Wire Line
	5000 3600 5000 2550
Connection ~ 5400 4250
Connection ~ 5400 4550
Wire Wire Line
	5400 4550 5400 3700
Wire Wire Line
	7850 3400 8100 3400
Wire Wire Line
	8100 3400 8100 4550
Wire Wire Line
	8100 4550 4800 4550
Wire Wire Line
	7450 3300 6800 3300
Connection ~ 4450 3750
Wire Wire Line
	4800 4550 4800 4100
Wire Wire Line
	4850 3300 5300 3300
Wire Wire Line
	5300 3200 5300 3100
Wire Wire Line
	5000 2550 6800 2550
Connection ~ 4900 3600
Wire Wire Line
	6800 2550 6800 3300
Wire Wire Line
	4800 4100 4450 4100
Wire Wire Line
	5300 2700 4450 2700
Wire Wire Line
	4450 2700 4450 4100
Connection ~ 4450 3300
Wire Wire Line
	7850 3600 8250 3600
Wire Wire Line
	8250 3600 8250 2900
Wire Wire Line
	8250 2900 5400 2900
Wire Wire Line
	5400 2900 5400 3400
Wire Wire Line
	7450 3700 7450 4250
Wire Wire Line
	7450 4250 5400 4250
Connection ~ 7000 3300
Wire Wire Line
	5300 3300 5300 3600
Wire Wire Line
	5300 3600 5400 3600
Wire Wire Line
	6900 3700 7350 3700
Wire Wire Line
	7350 3700 7350 3400
Wire Wire Line
	7350 3400 7450 3400
Wire Wire Line
	7450 3600 7400 3600
Wire Wire Line
	7400 3600 7400 3450
Wire Wire Line
	7400 3450 6900 3450
Wire Wire Line
	6900 3450 6900 3500
Text Label 7050 3450 0    60   ~ 0
CLK
Text Label 6950 3600 0    60   ~ 0
DOUT
Text Label 7050 3700 0    60   ~ 0
DIN
Text Label 7250 2900 0    60   ~ 0
SHDN
$Comp
L MCP3002 U1
U 1 1 5213DD3A
P 6150 3550
F 0 "U1" H 6150 3450 50  0000 C CNN
F 1 "MCP3002" H 6150 3650 50  0000 C CNN
F 2 "MODULE" H 6150 3550 50  0001 C CNN
F 3 "DOCUMENTATION" H 6150 3550 50  0001 C CNN
	1    6150 3550
	1    0    0    -1  
$EndComp
Text Label 5050 3300 0    60   ~ 0
D1
Text Label 5300 3150 0    60   ~ 0
D0
NoConn ~ 7850 3700
NoConn ~ 7850 3500
NoConn ~ 7850 3300
Text Label 5000 4550 0    60   ~ 0
GND
$Comp
L CONN_5X2F J1
U 1 1 5213ABCF
P 7650 3500
F 0 "J1" H 7570 3780 40  0000 C CNN
F 1 "CONN_5X2F" H 7675 3200 40  0000 C CNN
	1    7650 3500
	1    0    0    -1  
$EndComp
Text Label 7050 3300 0    60   ~ 0
3.3v
$Comp
L TMP36 Q1
U 1 1 5213AE42
P 4650 3400
F 0 "Q1" H 4650 3251 40  0000 R CNN
F 1 "TMP36" H 4650 3550 40  0000 R CNN
F 2 "TO92" H 4490 3502 29  0000 C CNN
	1    4650 3400
	0    -1   -1   0   
$EndComp
$Comp
L TMP36 Q4
U 1 1 5213AE40
P 5200 2900
F 0 "Q4" H 5200 2751 40  0000 R CNN
F 1 "TMP36" H 5200 3050 40  0000 R CNN
F 2 "TO92" H 5040 3002 29  0000 C CNN
	1    5200 2900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
